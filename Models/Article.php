<?php

namespace App\Models;

use App\Interfaces\PayementInterface;

class Article
{
    private $nom;
    private $prix;

    public function __construct($nom, $prix)
    {
        $this->nom = $nom;
        $this->prix = $prix;
    }
    public function getNom()
    {
        return $this->nom;
    }
    public function getPrix()
    {
        return $this->prix;
    }
    public function payement(PayementInterface $methode)
    {
        $montant = $this->getPrix();
        $methode->payer($montant);
    }
}
