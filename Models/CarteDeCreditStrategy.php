<?php

namespace App\Models;

use App\Interfaces\PayementInterface;

class CarteDeCreditStrategy implements PayementInterface
{

    private  $numeroCarte;
    private $cryptogramme;
    private $dateExpiration;

    public function __construct($num, $crypto, $date)
    {
        $this->numeroCarte = $num;
        $this->cryptogramme = $crypto;
        $this->dateExpiration = $date;
    }


    public function payer($montant)
    {
        echo strval($montant).
        "euros payes par carte de credit", "\n";
    }


}
