<?php

namespace App\Models;
use App\Interfaces\PayementInterface;

class PaypalStrategy implements PayementInterface
{

    private $email;
    private $password;

    public  function __construct($email, $pass)
    {
        $this->email = $email;
        $this->password = $pass;
    }
    public function payer($montant){
        echo strval($montant).
        "euros payes par Paypal", "\n";
    }

}
