<?php

require('autoload.php');

use App\Models\Article;
use App\Models\PaypalStrategy;
use App\Models\CarteDeCreditStrategy;

$article1 = new Article("Livre : \'Tout savoir sur le pattern Strategy\'", 25);
$article2 = new Article("Piano steinway & sons d274", 155690);
$article1->payement(new PaypalStrategy("idExemple", "passwordExemple"));
echo 'pour l\'article 1';
echo '<br>';
//pay by credit card
$article2->payement(new CarteDeCreditStrategy("1234567890123456", "786", "12/15"));
echo 'pour l\'article 2';
